# CONTRIBUTING

If you feel like this small script can be better, feel free to correct things or add
useful enhancements. When you are done, create a PR and wait, since I do not exprect any
contributions I think it might take a while to come back to this project, but I'll do my
best.
