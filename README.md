# README

## The Script

The script is located in the `./scripts` directory. You should be able to execute the
script in a Unix-like environment.

### What it does

1. It stops all your Docker containers
2. removes stopped Docker containers
3. creates an images.list file containing all docker images listed by `docker images`
4. removes all Docker images
5. pulls the docker images from images.list

## Reason

Docker doesn't offer a reinstallation of the images out of the box. If the following
errors sound familiar, one solution to solve them is to remove all docker images from
the host you are executing docker from. Since I found removing and adding my favorit
docker images annoying I came up with this simple script.

```bash
docker: Error response from daemon: error creating overlay mount to /var/lib/docker/overlay2/9c176dd629892631e355758916e55a8b15a16b120fbad59c3addb570f4c-init/merged: no such file or directory.
See 'docker run --help'.
```

```bash
Service '[service-name]' failed to build: error creating overlay mount to /var/lib/docker/overlay2/689ea7399cb70451b1970797cd28de2df0b54361ce2f72432d872ce1e2e/merged: no such file or directory
```

If it didn't do the trick for you be sure to take a look at the [StackOverflow](https://stackoverflow.com/questions/30984569/error-error-creating-aufs-mount-to-when-building-dockerfile#35630425)
topic.

## Development Requirements

The project requires to have [Vagrant](https://www.vagrantup.com/) installed.
The virtual machine is running on linux alpine and additionally installs Docker
and pulls some small alpine images to test the scripts functionallity.

The `scripts` directory from the host is mounted into the virtual machine and is
reachable at `/scripts`.

### Contributing

If you want to contribute see [CONTRIBUTING](CONTRIBUTING.md)
