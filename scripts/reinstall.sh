#!/bin/bash
#
# Docker image reinstaller

# requires docker images
docker images &> /dev/null

if [[ ${?} -ne 0 ]]; then
  # the docker image command failed, because of reasons ?!?!42?!?!
  echo "You do not have permissions to run the 'docker images' command."
  exit 1
fi

# validate that a /tmp directory exists
if [[ ! -f "/tmp" ]]; then
  # directory doesn't exist, create it. Root permissions might be required
  mkdir -p /tmp
fi

# create temporary file to store the current images
docker images | awk '{printf $1 ":" $2 "\n"}' | sed '1d' >> /tmp/images.list

# stop all running containers
docker container stop $(docker ps -a -q)

# remove stopped containers
docker container rm $(docker ps -a -q)

# remove all images from the filesystem
docker image rmi -f $(docker images -a -q)

# pull the images from the temorarily created images.list file
cat /tmp/images.list | xargs -I@ docker image pull @

# remove temporary images.list file
if [[ -e "/tmp/images.list" ]]; then
  rm -rf /tmp/images.list
fi

exit 0
